﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MajorSystem
{
	public class Major
	{
		public static void GenerateWord()
		{

			var consonant1 = "n";
			var consonant2 = "l";

			var vowels = new List<string>();

			vowels.Add("a");
			vowels.Add("e");
			vowels.Add("i");
			vowels.Add("o");
			vowels.Add("u");
			vowels.Add("y");
			vowels.Add("å");
			vowels.Add("ä");
			vowels.Add("ö");
			vowels.Add("h");


			foreach (var vowel in vowels)
			{
				Console.Write(vowel);
				Console.WriteLine(consonant1);
			}

			Console.WriteLine();

			foreach (var vowel in vowels)
			{
				Console.Write(consonant1);
				Console.WriteLine(vowel);
			}

			Console.WriteLine();

			foreach (var vowel in vowels)
			{
				foreach (var vowel1 in vowels)
				{
					Console.Write(vowel);
					Console.Write(consonant1);
					Console.WriteLine(vowel1);
				}

			}

			Console.WriteLine();

			foreach (var vowel in vowels)
			{
				Console.Write(vowel);
				Console.Write(consonant1);
				Console.WriteLine(consonant2);
			}

			Console.WriteLine();

			foreach (var vowel in vowels)
			{
				Console.Write(vowel);
				Console.Write(consonant2);
				Console.WriteLine(consonant1);
			}

			Console.WriteLine();

			foreach (var vowel in vowels)
			{
				Console.Write(consonant1);
				Console.Write(consonant2);
				Console.WriteLine(vowel);
			}

			Console.WriteLine();

			foreach (var vowel in vowels)
			{
				Console.Write(consonant2);
				Console.Write(consonant1);
				Console.WriteLine(vowel);
			}

			Console.WriteLine();

			foreach (var vowel in vowels)
			{
				foreach (var vowel1 in vowels)
				{
					Console.Write(vowel);
					Console.Write(consonant1);
					Console.Write(vowel1);
					Console.WriteLine(consonant2);
				}

			}

			Console.WriteLine();

			foreach (var vowel in vowels)
			{
				foreach (var vowel1 in vowels)
				{
					Console.Write(vowel);
					Console.Write(consonant1);
					Console.Write(consonant2);
					Console.WriteLine(vowel1);
				}
			}

			Console.WriteLine();

			foreach (var vowel in vowels)
			{
				foreach (var vowel1 in vowels)
				{
					Console.Write(consonant1);
					Console.Write(consonant2);
					Console.Write(vowel);
					Console.WriteLine(vowel1);
				}
			}

			Console.WriteLine();

			foreach (var vowel in vowels)
			{
				foreach (var vowel1 in vowels)
				{
					Console.Write(consonant1);
					Console.Write(vowel);
					Console.Write(consonant2);
					Console.WriteLine(vowel1);
				}
			}

			Console.WriteLine();

			foreach (var vowel in vowels)
			{
				foreach (var vowel1 in vowels)
				{
					Console.Write(consonant1);
					Console.Write(vowel);
					Console.Write(vowel1);
					Console.WriteLine(consonant2);
				}
			}
		}
	}
}
