﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MajorSystem
{
	public class Major2
	{
		public static void GenerateWord()
		{

			var characters = new List<string>();
			var words= new List<string>();

			//consonats
			characters.Add("n");
			characters.Add("l");
			
			//Voels
			characters.Add("a");
			characters.Add("e");
			characters.Add("i");
			characters.Add("o");
			characters.Add("u");
			characters.Add("y");
			characters.Add("å");
			characters.Add("ä");
			characters.Add("ö");
			characters.Add("h");

			for (var i = 0; characters.Count > i; i++)
			{
				string build = "";

				build += characters[i];
				words.Add(build);

				for (var j = 0; characters.Count > j; j++)
				{
					build = characters[i];
					build += characters[j];
					words.Add(build);
				}
			}

			words.Sort();
			
			foreach (string word in words)
			{
				Console.WriteLine(word);
			}
		}
	}
}
